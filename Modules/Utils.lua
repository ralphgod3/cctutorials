local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
local Dbg = require("Modules.Logger")
local PC = require("Modules.ParamCheck")

local Utils = {}

---convert string to byte array
---@param stringToConvert string string to convert into byte array
---@return number[] byteArray byte array representing input string
function Utils.convertStringToByteArray(stringToConvert)
	PC.expect(1, stringToConvert, "string")
	local tempArr = {}
	for i = 1, string.len(stringToConvert) do
		table.insert(tempArr, string.byte(stringToConvert, i, i))
	end
	return tempArr
end

---convert byte array into string
---@param arrayToConvert number[] byte array to convert into string
---@return string string converted byte array
function Utils.convertByteArrayToString(arrayToConvert)
	PC.expect(1, arrayToConvert, "table")
	local ret = {}
	for i = 1, #arrayToConvert do
		ret[i] = string.char(arrayToConvert[i])
	end
	return table.concat(ret)
end

---convert byte array into nibble array
---@param arrayToConvert number[] byte array to convert
---@return number[] nibbleArray converted nibble array
function Utils.convertByteArrayToNibbleArray(arrayToConvert)
	PC.expect(1, arrayToConvert, "table")
	local retTable = {}
	for _, v in ipairs(arrayToConvert) do
		if v > 255 then
			error("invalid byte array given", 2)
		end
		table.insert(retTable, bit.band(v, 15))
		table.insert(retTable, bit.band(bit.brshift(v, 4), 15))
	end
	return retTable
end

---convert nibble array into byte array
---@param arrayToConvert number[] nibble array to convert
---@return number[] byteArray converted nibble array
function Utils.convertNibbleArrayToByteArray(arrayToConvert)
	PC.expect(1, arrayToConvert, "table")
	local retTable = {}
	if #arrayToConvert % 2 ~= 0 then
		error("nibble array has uneven length", 2)
	end
	for i = 1, #arrayToConvert, 2 do
		if arrayToConvert[i] > 15 or arrayToConvert[i + 1] > 15 then
			error("invallid nibble array given", 2)
		end
		local byte = arrayToConvert[i]
		byte = byte + bit.blshift(arrayToConvert[i + 1], 4)
		table.insert(retTable, byte)
	end
	return retTable
end

---convert byte array into dibit array
---@param arrayToConvert number[] byte array to convert
---@return number[] dibit array
function Utils.convertByteArrayToDibitArray(arrayToConvert)
	PC.expect(1, arrayToConvert, "table")
	local retTable = {}
	for _, v in ipairs(arrayToConvert) do
		if v > 255 then
			error("invalid byte array given", 2)
		end
		table.insert(retTable, bit.band(v, 3))
		table.insert(retTable, bit.band(bit.brshift(v, 2), 3))
		table.insert(retTable, bit.band(bit.brshift(v, 4), 3))
		table.insert(retTable, bit.band(bit.brshift(v, 6), 3))
	end
	return retTable
end

---converts a dibit array back into a byte array
---@param arrayToConvert number[] dibit array to convert
---@return number[] byteArray converted array
function Utils.convertDibitArrayToByteArray(arrayToConvert)
	PC.expect(1, arrayToConvert, "table")
	local retTable = {}
	if #arrayToConvert % 4 ~= 0 then
		error("Dibit array not dividable by 4", 2)
	end
	for i = 1, #arrayToConvert, 4 do
		if arrayToConvert[i] > 3 or arrayToConvert[i + 1] > 3 or arrayToConvert[i + 2] > 3 or arrayToConvert[i + 3] > 3 then
			error("invallid dibit array given", 2)
		end
		local byte = arrayToConvert[i]
		byte = byte + bit.blshift(arrayToConvert[i + 1], 2)
		byte = byte + bit.blshift(arrayToConvert[i + 2], 4)
		byte = byte + bit.blshift(arrayToConvert[i + 3], 6)
		table.insert(retTable, byte)
	end
	return retTable
end



---finds an element in a table returns key for element if element exists or nil on failure
---@param tableToFindElementIn table table element will searched for
---@param element any element to find key for
---@return any key key or nil when element does not exist in table
function Utils.findElementInTable(tableToFindElementIn, element)
    PC.expect(1, tableToFindElementIn, "table")
    for k, v in pairs(tableToFindElementIn) do
        if v == element then
            return k
        end
    end
    return nil
end

---yields program if not given lastYieldTime, if given only yields when nescesary
---@param lastYieldTime number time from epoch that yield got called last time
---@return number lastYieldTime number that can be fed into next call of yield function
function Utils.yield(lastYieldTime)
	if not lastYieldTime or os.epoch("local") - lastYieldTime > 5000 then
		local myEvent = tostring({})
		os.queueEvent(myEvent)
		os.pullEvent(myEvent)
		return os.epoch("local")
	end
	return lastYieldTime
end


---uses deep copy to copy a table
---@param tableToCopy table table to copy
---@return table copy table copy
function Utils.copyTable(tableToCopy)
	PC.expect(1, tableToCopy, "table")
	local copy = {}
	for k, v in pairs(tableToCopy) do
		if type(v) == "table" then
			copy[k] = Utils.copyTable(v)
		else
			copy[k] = v
		end
	end
	return copy
end


---get amount of elements in table not indexed by number
---@param tableToCheck table to get length for
---@return number tableLen amount of elements in table
function Utils.getTableLength(tableToCheck)
    PC.expect(1, tableToCheck, "table")
    local len = 0
    for _, _ in pairs(tableToCheck) do
        len = len + 1
    end
    return len
end

---creates a binary semaphore and returns value
---@return BinarySemaphore binarySemaphore binary semaphore
function Utils.createBinarySemaphore()
	---@class BinarySemaphore
	return {false}
end

---takes binary semaphore if available, waits for it to be available otherwise
---@param sem BinarySemaphore binary semaphore created with createBinarySemaphore
---@return boolean semaphoreValue always true for now
function Utils.takeBinarySemaphore(sem)
	while sem[1] do
		Utils.yield()
	end
	sem[1] = true
	return sem[1]
end

---free binary semaphore
---@param sem BinarySemaphore binary semaphore created with createBinarySemaphore
---@return boolean semaphoreValue always false for now
function Utils.freeBinarySemaphore(sem)
	sem[1] = false
	return sem[1]
end

---checks if binary semaphore is free
---@param sem BinarySemaphore binary semaphore created with createBinarySemaphore
---@return boolean semaphoreFree true if free, false if taken
function Utils.isBinarySemaphoreFree(sem)
	return not sem[1]
end

---takes binary semaphore if available, returns false if it could not take the semaphore
---@param sem BinarySemaphore binary semaphore created with createBinarySemaphore
---@return boolean tookSemaphore true if it took the semaphore, false otherwise
function Utils.takeBinarySemaphoreIfFree(sem)
	if sem[1] == false then
		sem[1] = true
		return true
	end
	return false
end

---create counting semaphore
---@return CountingSemaphore countingSemaphore
function Utils.createCountingSemaphore()
	---@class CountingSemaphore
	return {0}
end

---take a counting semaphore (adds +1 to it)
---@param sem CountingSemaphore counting semaphore created with createCountingSemaphore
---@return number semaphoreValue value of the counting semaphote
function Utils.takeCountingSemaphore(sem)
	sem[1] = sem[1] + 1
	return sem[1]
end

---free a counting semaphore (subtracts 1 from it)
---@param sem CountingSemaphore counting semaphore created with createCountingSemaphore
---@return number semaphoreValue value of the counting semaphore
function Utils.freeCountingSemaphore(sem)
	sem[1] = sem[1] - 1
	return sem[1]
end

---wait while counting semaphore > 0
---@param sem CountingSemaphore counting semaphore created with createCountingSemaphore
function Utils.awaitCountingSemaphore(sem)
	while sem[1] > 0 do
		Utils.yield()
	end
	return sem[1]
end

---runs threading manager if nescesary to paralelize inventory access
---@param threads Ithreads implementation of Ithreads.lua interface
---@return boolean threadingManagerNeededRunning true if we ran threading manager, false otherwise
function Utils.runThreadingManagerIfNeeded(threads)
	if threads.isRunning == nil then
		error("threading manager missing isRunning method", 2)
	end
	if threads.startRunning == nil then
		error("threading manager missing startRunning method", 2)
	end

	if not threads.isRunning() then
		threads.startRunning()
		return true
	end
	return false
end















return Utils