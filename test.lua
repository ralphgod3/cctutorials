local Dbg = require("Modules.Logger")
local PC = require("Modules.ParamCheck")

Dbg.setOutputTerminal(term.current())

local p = peripheral.wrap("ironchest:iron_chest_0")
local goldChest = "ironchest:gold_chest_2"

--Dbg.logNoTag(p)

local function getDetailedItemList(inventory)
    local retTable = {}

    local funcTable = {}
    for k,v in pairs(inventory.list()) do
        table.insert(funcTable, function ()
            retTable[k] = inventory.getItemDetail(k)
        end)
    end

    parallel.waitForAll(table.unpack(funcTable))
    return retTable
end
--Dbg.logNoTag(getDetailedItemList(p))




while true do
    local items = p.list()
    for k,v in pairs(items) do
        if v.name == "minecraft:cobblestone" then
            p.pushItems(goldChest, k)
        end
    end
    sleep(1)
end